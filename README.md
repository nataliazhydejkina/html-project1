![LAYOUT.png](https://raw.githubusercontent.com/greenmoon1558/html-project1/master/pantomax_lp_new.psd.png)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run start
```

### Compiles and minifies for production
```
npm run build
```